namespace _Project.Scripts.Features.Person
{
  public interface IPersonLogic
  {
    void Construct();
    void Clear();
    void SetMovementSpeed(float speed);
    void SetRotationSpeed(float speed);
    void SetMaxPointInQueue(uint length);
  }
}