using UnityEngine;
using UnityEngine.AI;

namespace _Project.Scripts.Features.Person.Mono
{
  [RequireComponent(typeof(NavMeshAgent))]
  public class PersonView : MonoBehaviour
  {
    public NavMeshAgent MeshAgent;
  }
}