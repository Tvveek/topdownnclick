using _Project.Scripts.Features.Person.Mono;
using _Project.Scripts.Services._Interfaces;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

namespace _Project.Scripts.Features.Person
{
  public class PersonLogic : IPersonLogic
  {
    private readonly PersonView _personView;
    private readonly ICameraService _cameraService;
    private readonly IGameSaveSystem _gameSaveSystem;

    private Tween _tween;
    private Camera _camera;
    private RaycastHit _raycastHit;

    private uint _movementPointsLength = 2;

    public PersonLogic(PersonView personView, ICameraService cameraService, IGameSaveSystem gameSaveSystem)
    {
      _personView = personView;
      _cameraService = cameraService;
      _gameSaveSystem = gameSaveSystem;
    }

    public void Construct()
    {
      CreateCamera();
      LoadPersonDestination();
      MovementOnClick();

      Application.quitting += OnApplicationQuit;
    }

    public void Clear()
    {
      _tween.Kill();
      _cameraService.Clear();
      OnApplicationQuit();
    }

    public void SetMovementSpeed(float speed) =>
      _personView.MeshAgent.speed = speed;

    public void SetRotationSpeed(float speed) =>
      _personView.MeshAgent.angularSpeed = speed;

    public void SetMaxPointInQueue(uint length) => 
      _movementPointsLength = length;

    private void CreateCamera()
    {
      _camera = Camera.main;
      _cameraService.CreateCinemachineVirtualCamera(_personView.transform);
    }

    private void LoadPersonDestination()
    {
      if (_gameSaveSystem.Get().MovementDirection.Count == 0)
        _gameSaveSystem.Get().MovementDirection.Add(_personView.transform.position);

      _personView.MeshAgent.SetDestination(_gameSaveSystem.Get().MovementDirection[0]);
    }

    private void MovementOnClick()
    {
      _tween = DOTween.To(() => 0, x =>
        {
          if (_gameSaveSystem.Get().MovementDirection.Count > 1)
          {
            if (_personView.MeshAgent.remainingDistance <= _personView.MeshAgent.stoppingDistance)
            {
              _gameSaveSystem.Get().MovementDirection.RemoveAt(0);
              _personView.MeshAgent.SetDestination(_gameSaveSystem.Get().MovementDirection[0]);
            }
          }

          if (EventSystem.current.currentSelectedGameObject != null)
            return;

          if (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
            CheckDestination();
        }, 1, 1)
        .SetUpdate(true)
        .SetLoops(-1, LoopType.Restart);
    }

    private void CheckDestination()
    {
      Vector3 inputPosition;

      if (Input.touchCount > 0)
        inputPosition = Input.GetTouch(0).position;
      else
        inputPosition = Input.mousePosition;

      Ray ray = _camera.ScreenPointToRay(inputPosition);
      if (!Physics.Raycast(ray, out RaycastHit hit))
        return;

      if (NavMesh.SamplePosition(hit.point, out NavMeshHit navHit, 1.0f, NavMesh.AllAreas))
      {
        // Debug.Log("Adding move point: " + navHit.position);
        if (_gameSaveSystem.Get().MovementDirection.Count < _movementPointsLength)
          _gameSaveSystem.Get().MovementDirection.Add(navHit.position);
      }
      else
        Debug.Log("Point doesn't in the grid");
    }

    private void OnApplicationQuit()
    {
      Application.quitting -= OnApplicationQuit;
      _gameSaveSystem.Get().LastRotation = (_personView.transform.rotation);
      _gameSaveSystem.Get().LastPosition = (_personView.transform.position);
      _gameSaveSystem.Save();

      Debug.Log("Save last person position");
    }
  }
}