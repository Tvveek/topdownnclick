using System;
using _Project.Scripts.Features.LevelMap.Mono;
using UnityEngine.AI;

namespace _Project.Scripts.Features.LevelMap
{
  public class LevelLogic : ILevelLogic
  {
    private readonly LevelView _levelView;

    private NavMeshDataInstance _mNavMeshInstance;
    
    private Action _onComplete;

    public LevelLogic(LevelView levelView) =>
      _levelView = levelView;

    public void Construct() => 
      _mNavMeshInstance = NavMesh.AddNavMeshData(_levelView.MNavMeshData);

    public void Clear() =>
      NavMesh.RemoveNavMeshData(_mNavMeshInstance);
  }
}