namespace _Project.Scripts.Features.LevelMap
{
  public interface ILevelLogic
  {
    void Construct();
    void Clear();
  }
}