using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.HomeScreen.Mono
{
  public class HomeViewMono : MonoBehaviour
  {
    public Button StartButton;
    public TMP_Dropdown LocationDropdown;
    public TMP_Dropdown SaverDropdown;
  }
}