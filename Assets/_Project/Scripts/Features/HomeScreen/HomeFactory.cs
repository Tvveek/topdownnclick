using _Project.Scripts.Core_Logic.Common;
using _Project.Scripts.Features.HomeScreen.Mono;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.HomeScreen
{
  public class HomeFactory : GameObjectFactory, IHomeFactory
  {
    private const string BaseCanvas = "Canvas";
    private const string HomeViewPath = "Features/HomeScreen/HomeView";

    private HomeViewMono HomeViewMono { get; set; }
    public IHomeViewLogic HomeViewLogic { get; private set; }

    private readonly IAudioService _audioService;
    private readonly IGameSaveSystem _gameSaveSystem;
    
    public HomeFactory(IAssetProvider assetProvider, IAudioService audioService, IGameSaveSystem gameSaveSystem) : base(assetProvider)
    {
      _audioService = audioService;
      _gameSaveSystem = gameSaveSystem;
    }

    public void CreateHomeView()
    {
      ClearHomeView();
      HomeViewMono = Create<HomeViewMono>(HomeViewPath, GameObject.Find(BaseCanvas).transform);
      HomeViewLogic = new HomeViewLogic(HomeViewMono, _audioService, _gameSaveSystem);
    }

    public void ClearHomeView()
    {
      if (!HomeViewMono) return;
      
      Object.Destroy(HomeViewMono.gameObject);
    }
  }
}