using System;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.HomeScreen.Mono;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.SaveSystem;

namespace _Project.Scripts.Features.HomeScreen
{
  public class HomeViewLogic : IHomeViewLogic
  {
    private readonly HomeViewMono _homeViewMono;
    private readonly IAudioService _audioService;
    private readonly IGameSaveSystem _gameSaveSystem;
    
    public HomeViewLogic(HomeViewMono homeViewMono, IAudioService audioService, IGameSaveSystem gameSaveSystem)
    {
      _homeViewMono = homeViewMono;
      _audioService = audioService;
      _gameSaveSystem = gameSaveSystem;
    }

    public void Construct(Action toGame)
    {
      _homeViewMono.StartButton.onClick.AddListener(toGame.Invoke);
      
      _homeViewMono.LocationDropdown.onValueChanged.AddListener(ChangeLocation);
      _homeViewMono.LocationDropdown.value = (int)_gameSaveSystem.Get().ActiveLocation;
      
      _homeViewMono.SaverDropdown.onValueChanged.AddListener(ChangeSaverType);
      _homeViewMono.SaverDropdown.value = (int)_gameSaveSystem.Get().SavingType;
      
      CreateAnimations();
      CreateSounds();
      
      _audioService.Play(AudioId.Music_OST_Standart);
    }

    private void ChangeLocation(int index)
    {
      _gameSaveSystem.Get().ActiveLocation = (uint)index;
      _gameSaveSystem.Save();
    }
    
    private void ChangeSaverType(int index)
    {
      _gameSaveSystem.SetSaverType((SavingType) index);
      _gameSaveSystem.Get().SavingType = (SavingType) index;
      _gameSaveSystem.Save();
    }

    private void CreateAnimations() =>
      new ButtonAnimation(_homeViewMono.StartButton.transform);

    private void CreateSounds() =>
      new ButtonSound(_audioService, _homeViewMono.StartButton.gameObject, AudioId.Click);
  }
}