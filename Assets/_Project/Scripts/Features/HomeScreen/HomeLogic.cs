using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Services._Interfaces;

namespace _Project.Scripts.Features.HomeScreen
{
  public class HomeLogic
  {
    private readonly IHomeFactory _homeFactory;
    private readonly IGameStateMachine _gameStateMachine;
    
    public HomeLogic(IGameStateMachine gameStateMachine, IAssetProvider assetProvider, IAudioService audioService, IGameSaveSystem gameSaveSystem)
    {
      _gameStateMachine = gameStateMachine;
      _homeFactory = new HomeFactory(assetProvider, audioService, gameSaveSystem);
    }
    
    public void Enter()
    {
      CreateHomeView();
    }

    public void CloseHomeView() => 
      _homeFactory.ClearHomeView();

    private void CreateHomeView()
    {
      _homeFactory.CreateHomeView();
      _homeFactory.HomeViewLogic.Construct(ToGame);
    }

    private void ToGame() => 
      _gameStateMachine.Enter<LoadGameState>();
  }
}