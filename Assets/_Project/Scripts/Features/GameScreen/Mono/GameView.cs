using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.GameScreen.Mono
{
  public class GameView : MonoBehaviour
  {
    public Button HomeButton;

    public Slider MovementSpeedSlider;
    public Slider RotationSpeedSlider;
    
    public Slider MaxPointsInQueueSlider;
    public TextMeshProUGUI MaxPointsInQueueTitle;
  }
}