using System.Threading.Tasks;
using _Project.Scripts.Features.LevelMap;
using _Project.Scripts.Features.Person;

namespace _Project.Scripts.Features.GameScreen
{
  public interface IGameFactory
  {
    IGameViewLogic GameViewLogic { get;}
    ILevelLogic LevelLogic { get;}
    IPersonLogic PersonLogic { get;}
    void CreateGameView();
    Task CreateLevel();
    Task CreatePerson();
    void ClearGameView();
  }
}