using System;
using System.Threading.Tasks;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.GameScreen.Mono;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;

namespace _Project.Scripts.Features.GameScreen
{
  public class GameViewLogic : IGameViewLogic
  {
    private readonly IAudioService _audioService;
    private readonly IGameSaveSystem _gameSaveSystem;

    private readonly IGameFactory _gameFactory;

    private readonly GameView _gameView;

    public GameViewLogic(IGameFactory gameFactory, GameView gameView, IAudioService audioService, IGameSaveSystem gameSaveSystem)
    {
      _gameView = gameView;
      _gameFactory = gameFactory;
      _audioService = audioService;
      _gameSaveSystem = gameSaveSystem;
    }

    public async void Construct(Action toHome)
    {
      CreateAnimations();
      CreateSounds();

      await CreateLevel();
      await CreatePerson();

      AddListeners(toHome);
    }

    private void AddListeners(Action toHome)
    {
      _gameView.HomeButton.onClick.AddListener(toHome.Invoke);

      _gameView.MovementSpeedSlider.onValueChanged.AddListener(OnChangeMovementSpeed);
      _gameView.RotationSpeedSlider.onValueChanged.AddListener(OnChangeRotationSpeed);
      _gameView.MaxPointsInQueueSlider.onValueChanged.AddListener(OnChangeMaxPointInQueue);
    }

    private async Task CreateLevel()
    {
      await _gameFactory.CreateLevel();
      _gameFactory.LevelLogic.Construct();
    }

    private async Task CreatePerson()
    {
      await _gameFactory.CreatePerson();
      _gameFactory.PersonLogic.Construct();
    }

    private void OnChangeMovementSpeed(float speed) =>
      _gameFactory.PersonLogic.SetMovementSpeed(speed);

    private void OnChangeRotationSpeed(float speed) =>
      _gameFactory.PersonLogic.SetRotationSpeed(speed * 100);

    private void OnChangeMaxPointInQueue(float length)
    {
      _gameFactory.PersonLogic.SetMaxPointInQueue((uint) length);
      _gameView.MaxPointsInQueueTitle.text = "Max Points In Queue : " + length;
    }

    private void CreateAnimations() =>
      new ButtonAnimation(_gameView.HomeButton.transform);

    private void CreateSounds() =>
      new ButtonSound(_audioService, _gameView.HomeButton.gameObject, AudioId.Click);
  }
}