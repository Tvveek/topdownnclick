using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Services._Interfaces;

namespace _Project.Scripts.Features.GameScreen
{
  public class GameLogic
  {
    private readonly IGameFactory _gameFactory;
    private readonly IGameStateMachine _gameStateMachine;

    public GameLogic(IGameStateMachine gameStateMachine, IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem, 
      IAudioService audioService, ICameraService cameraService)
    {
      _gameStateMachine = gameStateMachine;
      _gameFactory = new GameFactory(assetProvider, gameSaveSystem, audioService, cameraService);
    }

    public void Enter() =>
      CreateGameView();

    public void CloseGameView() => 
      _gameFactory.ClearGameView();

    private void CreateGameView()
    {
      _gameFactory.CreateGameView();
      _gameFactory.GameViewLogic.Construct(ToHome);
    }

    private void ToHome() =>
      _gameStateMachine.Enter<LoadHomeState>();
  }
}