using System.Threading.Tasks;
using _Project.Scripts.Core_Logic.Common;
using _Project.Scripts.Features.GameScreen.Mono;
using _Project.Scripts.Features.LevelMap;
using _Project.Scripts.Features.LevelMap.Mono;
using _Project.Scripts.Features.Person;
using _Project.Scripts.Features.Person.Mono;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.GameScreen
{
  public class GameFactory : GameObjectFactory, IGameFactory
  {
    private const string BaseCanvas = "Canvas";

    private const string LevelAddress = "LevelMap_";
    private const string PersonAddress = "Person";
    private const string GameViewPath = "Features/GameScreen/GameView";

    private readonly IAudioService _audioService;
    private readonly ICameraService _cameraService;
    private readonly IGameSaveSystem _gameSaveSystem;

    private GameView GameView { get; set; }
    private GameObject LevelView { get; set; }
    private GameObject PersonView { get; set; }
    public IGameViewLogic GameViewLogic { get; private set; }
    public ILevelLogic LevelLogic { get; private set; }
    public IPersonLogic PersonLogic { get; private set; }

    public GameFactory(IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem, IAudioService audioService,
      ICameraService cameraService) : base(assetProvider)
    {
      _audioService = audioService;
      _cameraService = cameraService;
      _gameSaveSystem = gameSaveSystem;
    }

    public void CreateGameView()
    {
      GameView = Create<GameView>(GameViewPath, GameObject.Find(BaseCanvas).transform);
      GameViewLogic = new GameViewLogic(this, GameView, _audioService, _gameSaveSystem);
    }

    public async Task CreateLevel()
    {
      LevelView = await CreateAsync(LevelAddress + _gameSaveSystem.Get().ActiveLocation);
      LevelLogic = new LevelLogic(LevelView.GetComponent<LevelView>());

      await Task.CompletedTask;
    }

    public async Task CreatePerson()
    {
      PersonView = await CreateAsync(PersonAddress, _gameSaveSystem.Get().LastPosition, _gameSaveSystem.Get().LastRotation);
      PersonLogic = new PersonLogic(PersonView.GetComponent<PersonView>(), _cameraService, _gameSaveSystem);

      await Task.CompletedTask;
    }

    public void ClearGameView()
    {
      LevelLogic.Clear();
      PersonLogic.Clear();

      if (GameView)
        Object.Destroy(GameView.gameObject);
      
      if (PersonView)
        Object.Destroy(PersonView.gameObject);
      
      if (LevelView)
        Object.Destroy(LevelView.gameObject);

      Release(LevelAddress);
      Release(PersonAddress);
    }
  }
}