using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Camera;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.SaveSystem;
using Zenject;

namespace _Project.Scripts.Core_Logic.Zenject
{
  public class ServicesRegistration : MonoInstaller
  {
    public override void InstallBindings()
    {
      BindSingleService<IAssetProvider, AssetProvider>();
      BindSingleService<IAudioService, AudioService>();
      BindSingleService<IGameSaveSystem, GameSaveSystem>();
      BindSingleService<IInputResolver, InputResolver>();
      BindSingleService<ICameraService, CameraService>();
    }

    private void BindSingleService<T, T2>() where T2 : T => 
      Container.Bind<T>().To<T2>().AsSingle();
  }
}