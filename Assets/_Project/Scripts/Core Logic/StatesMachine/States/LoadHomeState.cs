using System;
using System.Threading.Tasks;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Features.HomeScreen;
using _Project.Scripts.Services._Interfaces;

namespace _Project.Scripts.Core_Logic.StatesMachine.States
{
  public class LoadHomeState : IState
  {
    private readonly IAudioService _audioService;
    private readonly IAssetProvider _assetProvider;
    private readonly IGameSaveSystem _gameSaveSystem;

    private IGameStateMachine _gameStateMachine;

    private HomeLogic _homeLogic;

    public LoadHomeState(IAssetProvider assetProvider, IAudioService audioService, IGameSaveSystem gameSaveSystem)
    {
      _audioService = audioService;
      _assetProvider = assetProvider;
      _gameSaveSystem = gameSaveSystem;
    }

    public void Construct(IGameStateMachine gameStateMachine) =>
      _gameStateMachine = gameStateMachine;

    public void Enter()
    {
      _homeLogic = new HomeLogic(_gameStateMachine, _assetProvider, _audioService, _gameSaveSystem);
      _homeLogic.Enter();
    }

    public void Exit() =>
      _homeLogic.CloseHomeView();

    public Task AsyncEnter() =>
      throw new NotImplementedException();
  }
}