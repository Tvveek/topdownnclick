using System;
using System.Threading.Tasks;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Features.GameScreen;
using _Project.Scripts.Services._Interfaces;

namespace _Project.Scripts.Core_Logic.StatesMachine.States
{
  public class LoadGameState : IState
  {
    private readonly IAudioService _audioService;
    private readonly IAssetProvider _assetProvider;
    private readonly ICameraService _cameraService;
    private readonly IGameSaveSystem _gameSaveSystem;

    private IGameStateMachine _gameStateMachine;

    private GameLogic _gameLogic;

    public LoadGameState(IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem, IAudioService audioService, ICameraService cameraService)
    {
      _audioService = audioService;
      _assetProvider = assetProvider;
      _cameraService = cameraService;
      _gameSaveSystem = gameSaveSystem;
    }

    public void Construct(IGameStateMachine gameStateMachine) =>
      _gameStateMachine = gameStateMachine;

    public void Enter()
    {
      _gameLogic = new GameLogic(_gameStateMachine, _assetProvider, _gameSaveSystem, _audioService, _cameraService);
      _gameLogic.Enter();
    }

    public void Exit() =>
      _gameLogic.CloseGameView();

    public Task AsyncEnter() =>
      throw new NotImplementedException();
  }
}