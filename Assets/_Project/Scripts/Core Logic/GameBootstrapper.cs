using System;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Features.Splashscreen;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Core_Logic
{
  public class GameBootstrapper : MonoBehaviour
  {
    private IAssetProvider _assetProvider;

    private IGameStateMachine _gameStateMachine;
    private SplashLogic _splashLogic;

    private void Awake()
    {
      Application.targetFrameRate = 120;
      LaunchWithSplash(LaunchGameStateMachine);
    }

    [Inject]
    public void Construct(IAssetProvider assetProvider, IGameStateMachine gameStateMachine)
    {
      _assetProvider = assetProvider;
      _gameStateMachine = gameStateMachine;
    }

    private void LaunchWithSplash(Action initializeGame)
    {
      _splashLogic = new SplashLogic(_assetProvider);
      _splashLogic.Launch(initializeGame);

      _splashLogic.Complete();
    }

    private void LaunchGameStateMachine()
    {
      _splashLogic.Clear();
      _gameStateMachine.Enter<LoadGameServicesState>();
    }
  }
}