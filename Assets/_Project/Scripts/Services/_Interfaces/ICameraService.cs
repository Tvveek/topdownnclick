using UnityEngine;

namespace _Project.Scripts.Services._Interfaces
{
  public interface ICameraService
  {
    void CreateCinemachineVirtualCamera(Transform followPerson);
    void Clear();
  }
}