using _Project.Scripts.Services._Interfaces;
using Cinemachine;
using UnityEngine;

namespace _Project.Scripts.Services.Camera
{
  public class CameraService : ICameraService
  {
    private const string VirtualCameraPath = "Features/GameScreen/Virtual Camera";
    
    private readonly IAssetProvider _assetProvider;
    private readonly Vector3 _cameraBoundsSize = new(50, 50, 50);
    
    private CinemachineVirtualCamera _camera;
    private BoxCollider _cameraBounds;

    public CameraService(IAssetProvider assetProvider) =>
      _assetProvider = assetProvider;

    public void CreateCinemachineVirtualCamera(Transform followPerson)
    {
      AddCameraBounds();
      
      _camera = CreateVirtualCamera();
      _camera.Follow = followPerson;
      _camera.GetComponent<CinemachineConfiner>().m_BoundingVolume = _cameraBounds;
    }

    public void Clear()
    {
      if(_camera)
        Object.Destroy(_camera.gameObject);

      _camera = null;
    }

    private void AddCameraBounds()
    {
      if (_cameraBounds != null)
        return;

      var gameObject = new GameObject("Camera bounds");
      _cameraBounds = gameObject.AddComponent<BoxCollider>();
      _cameraBounds.size = _cameraBoundsSize;
    }

    private CinemachineVirtualCamera CreateVirtualCamera()
    {
      var original = _assetProvider.GetResource<CinemachineVirtualCamera>(VirtualCameraPath);
      CinemachineVirtualCamera instance = Object.Instantiate(original);
      return instance;
    }
  }
}