using System.Collections.Generic;
using _Project.Scripts.Services.SaveSystem;
using UnityEngine;

namespace _Project.Scripts.Services._data
{
  public class GameSaveData
  {
    public bool IsMusicDisabled;
    public bool IsSoundDisabled;
    
    public uint ActiveLocation = 1;
    public SavingType SavingType = SavingType.Local;

    public Vector3 LastPosition = new();
    public Quaternion LastRotation = new();
    public List<Vector3> MovementDirection = new();
  }
}