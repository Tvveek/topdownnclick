namespace _Project.Scripts.Services.Audio.Enum
{
public enum AudioId
{
	Default,
	Click,
	ClickBack,
	StartButton,
	WellDone,
	Win,
	Music_OST_Slow,
	Music_OST_Standart,
	Music_OST_Fast,
	Tile,
	StandFigure
}
}
