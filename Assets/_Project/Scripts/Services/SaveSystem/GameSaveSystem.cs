using System;
using System.Threading.Tasks;
using _Project.Scripts.Services._data;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Services.SaveSystem
{
  public class GameSaveSystem : IGameSaveSystem
  {
    private const string SaveKey = "GameSaveData.1";

    private GameSaveData _gameSaveData;
    private SavingType _savingType;

    public GameSaveSystem()
    {
      string json = PlayerPrefs.GetString(SaveKey);
      _gameSaveData = JsonUtility.FromJson<GameSaveData>(json);
    }

    public GameSaveData Get()
    {
      if (_savingType == SavingType.Remote)
      {
        Task.Delay(1000); //if it was like remote loading
        return _gameSaveData ??= new GameSaveData(); //return remote data
      }

      return _gameSaveData ??= new GameSaveData();
    }

    public async void Save()
    {
      if (_savingType == SavingType.Remote)
      {
        await Task.Delay(1000); //Remote saving
        PlayerPrefs.SetString(SaveKey, JsonUtility.ToJson(_gameSaveData)); //it doesn't for remote
      }
      else
        PlayerPrefs.SetString(SaveKey, JsonUtility.ToJson(_gameSaveData));
    }

    public void SetSaverType(SavingType type) =>
      _savingType = type;
  }
}